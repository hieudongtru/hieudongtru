﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PRN292Project.Startup))]
namespace PRN292Project
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
