namespace Project_PRN292.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("KetQua")]
    public partial class KetQua
    {
        [StringLength(50)]
        public string MaSV { get; set; }

        [StringLength(50)]
        public string MaMH { get; set; }

        public decimal? Diem_Lab1 { get; set; }

        public int ID { get; set; }

        public decimal? Diem_Lab2 { get; set; }

        public decimal? Diem_Lab3 { get; set; }

        public virtual SinhVien SinhVien { get; set; }
    }
}
