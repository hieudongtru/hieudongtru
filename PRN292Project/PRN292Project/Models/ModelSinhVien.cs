﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project_PRN292.Models
{
    public class ModelSinhVien
    {
        public string MaSV { get; set; }

        
        public string Ten { get; set; }

        
        public string NgaySinh { get; set; }

        public bool? GioiTinh { get; set; }

        public string Anh { get; set; }

        public string Email { get; set; }

    
        public string Password { get; set; }
    }
}