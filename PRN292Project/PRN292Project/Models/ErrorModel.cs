﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRN292Project.Models
{
    public class ErrorModel
    {
        public string ErrorCode { get; set; }
        public string ErrorMess { get; set; }
    }
}