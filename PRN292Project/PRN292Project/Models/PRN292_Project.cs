namespace PRN292Project.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Project_PRN292.Models;

    public partial class PRN292_Project : DbContext
    {
        public PRN292_Project()
            : base("name=PRN292Project")
        {
        }

        public virtual DbSet<DiemDanh> DiemDanhs { get; set; }
        public virtual DbSet<KetQua> KetQuas { get; set; }
        public virtual DbSet<MonHoc> MonHocs { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<NhanVien> NhanViens { get; set; }
        public virtual DbSet<SinhVien> SinhViens { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<KetQua>()
                .Property(e => e.Diem_Lab1)
                .HasPrecision(18, 0);

            modelBuilder.Entity<KetQua>()
                .Property(e => e.Diem_Lab2)
                .HasPrecision(18, 0);

            modelBuilder.Entity<KetQua>()
                .Property(e => e.Diem_Lab3)
                .HasPrecision(18, 0);

            modelBuilder.Entity<SinhVien>()
                .HasMany(e => e.KetQuas)
                .WithOptional(e => e.SinhVien)
                .HasForeignKey(e => e.MaMH);
        }

        public System.Data.Entity.DbSet<Project_PRN292.Models.LoginModel> LoginModels { get; set; }
    }
}
