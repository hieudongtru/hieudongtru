﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project_PRN292.Models
{
    public class LoginModel
    {
        [Key]
        [Display(Name = "Ten Dang Nhap")]
        [Required(ErrorMessage = "Yeu cau ban phai nhap tai khoan")]
        public string Username { get; set; }

        [Display(Name = "Mat Khau")]
        [Required(ErrorMessage = "Ban phai nhap mat khau")]
        public string Password { get; set; }
        public bool Reb { get; set; }
    }
}