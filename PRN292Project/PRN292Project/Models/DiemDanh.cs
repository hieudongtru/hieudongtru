namespace Project_PRN292.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DiemDanh")]
    public partial class DiemDanh
    {
        [StringLength(50)]
        public string MaSV { get; set; }

        [StringLength(50)]
        public string MaMH { get; set; }

        public DateTime? Ngay { get; set; }

        public int ID { get; set; }

        [Column("DiemDanh")]
        public bool? DiemDanh1 { get; set; }

        public virtual MonHoc MonHoc { get; set; }

        public virtual SinhVien SinhVien { get; set; }
    }
}
