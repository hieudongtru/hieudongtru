﻿let Url = window.location.origin;

const loginMethod = (rs) => {
    
    fetch(Url + "/Account/Login/", {
        method: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(rs)
    }).then(response => response.json()).then(response => ResultMethod(response));

}

$("#dangnhap").on("submit", function (e) {
    e.preventDefault();
    var temp = {
        Username:$("#email").val(),
        Password: $("#password").val()
    }
    loginMethod(temp);
});


const ResultMethod =(rs) => {
    if (rs.ErrorCode == 0) {
        window.location.href = Url + "/Home";
    } else {
        alert('Sai tài khoản hoặc mật khẩu !!');
    }
}