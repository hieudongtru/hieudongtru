﻿let url = window.location.origin;
const editSinhVien = (id) => {
    fetch(url + "/GetDataFromDatabase/GetDataSinhVienByID/" + id, {
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
        //body: JSON.stringify(temp)
    }).then(response => response.json()).then(response => BlindDataToModal(response));
}

const BlindDataToModal = (res) => {
    $("#studentName").val(res.Ten);
    $("#doblast").val(res.NgaySinh);
   
    $("#email").val(res.Email);
    if (res.GioiTinh == true) {
        $('input:radio[name="gender"][value="Male"]').prop('checked', true);
    } else {
        $('input:radio[name="gender"][value="Female"]').prop('checked', true);
    }

  
    
}

