﻿using PRN292Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRN292Project.Controllers
{   [Authorize]
    public class HomeController : Controller
    {

        private PRN292_Project _db;
        public HomeController()
        {
            _db = new PRN292_Project();
        }
        public ActionResult Index()
        {
            var temp = _db.SinhViens.Select(x=>x).ToList();
            TempData["MonHoc"] = _db.MonHocs.Select(x => x).ToList();
            TempData["News"] = _db.News.Select(x => x).ToList();
            return View(temp);
        }

        public ActionResult IndexMonHoc()
        {
         
            var temp = _db.MonHocs.Select(x => x).ToList();
            return View(temp);
        }

        public ActionResult Test(string id)
        {
            var temp = _db.SinhViens.Where(x => x.MaSV == id).SingleOrDefault();
            return View(temp);
        }

        public ActionResult AddStudent(String id)
        {
            var temp = _db.MonHocs.Where(x => x.MaMH == id).SingleOrDefault();
            return View(temp);
        }
    }
}