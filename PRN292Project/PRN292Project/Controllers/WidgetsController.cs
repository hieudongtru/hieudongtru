﻿using PRN292Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PRN292Project.Controllers
{
    public class WidgetsController : Controller
    {
        // GET: Widgets
        private readonly PRN292_Project _db;
        public WidgetsController()
        {
            _db = new PRN292_Project();
        }
        public PartialViewResult WidgetsPartial()
        {
            ViewBag.TongHocSinh = _db.SinhViens.Count();
            ViewBag.TongMonHoc = _db.MonHocs.Count();
            ViewBag.TongBaiViet = _db.News.Count();
            ViewBag.TongAbsent = _db.DiemDanhs.Where(x => x.DiemDanh1 == false).Count();
            return PartialView();
            
        }
    }
}